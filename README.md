# calculator

Multi-module RMI Calculator Project

# Running this example

To compile the code:

```
$ mvn install
```

To run the server, create a terminal and write:

```
$ mvn -pl server exec:java
```

To run the client, create another terminal and write:

```
$ mvn -pl client exec:java
```
